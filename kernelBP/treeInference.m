%Exact inference on trees via nonparametric kernel message passing

%Implements the algorithm in the paper:
%Le Song, Arthur Gretton, Carlos Guestrin
%Nonparametric Tree Graphical Models via Kernel Embeddings. 
%AISTATS 2010

%Le Song, Arthur Gretton, Carlos Guestrin
%12/09/2010

%INPUTS:

% A: matrix encoding directed edges between nodes
% rootNode: index of the root node
% Karr: matrix containing Gram matrices for each edge
% leafArr: training observations at each leaf
% observedList: list of observed leaf nodes
% observations: observations at the observed leaf nodes
% msgPrm: parameters for the nonparametric inference procedure
%         (regularization, kernel size)

%OUTPUTS:

%betaArr: array of beta values encoding the messages passed to the root

function [betaArr] = treeInference(A,rootNode,Karr,leafArr,observedList,observations,msgPrm)

numNodes = size(A,1);  
  
%Prune away unobserved leaves
[A,prunedList] = pruneTree(A,observedList);


%Compute OBSERVED LEAF messages

for leafInd=1:length(observedList)
  whichLeaf = observedList(leafInd);
  
  parentInd = find(A(:,whichLeaf)>0);
  Kt = Karr{whichLeaf,whichLeaf};
  Ks = Karr{whichLeaf,parentInd};
  I = eye(size(Kt));

  kt = rbf_dot( leafArr{whichLeaf},observations(leafInd),msgPrm.sig );
  betaArr(whichLeaf) = { ((Kt + msgPrm.lambda*I)*(Ks+msgPrm.lambda*I)) \kt };
  
end



%Compute INTERNAL messages

%Nodes for which outgoing message has been computed. So far, these
%are the observed leaves.
computedList = observedList;          

numUpdates = 1;
while numUpdates>0
  numUpdates = 0;


  for nodeInd = setdiff(1:numNodes,[computedList prunedList rootNode])

    childInd = find(A(nodeInd,:)>0);
    parentInd = find(A(:,nodeInd)>0);
    
    %If all  children of a node are computed, compute the message
    %to the parent, add the node to computedList, increment
    %numUpdates to indicate a new message was computed this iteration.
    
    if length(intersect( childInd ,computedList))==length(childInd)

      
      Ks = Karr{nodeInd,parentInd};
      nts = size(Ks,1); %n for edge (ts)
      Ktu_Beta = ones(nts,1);

      for whichChild = childInd

        Ktu_Beta = Ktu_Beta.* (Karr{nodeInd,whichChild}*...
                   betaArr{whichChild});
       
      end
        

      betaArr(nodeInd) = { ( Ks + msgPrm.lambda*eye(nts)) \ Ktu_Beta };
      computedList = [computedList nodeInd];
      numUpdates = numUpdates+1;
    end
    
  end  
end

