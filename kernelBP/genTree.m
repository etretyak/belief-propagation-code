%Demo of exact inference on trees using kernel message passing.

%Implements the algorithm in the paper:
%Le Song, Arthur Gretton, Carlos Guestrin
%Nonparametric Tree Graphical Models via Kernel Embeddings. 
%AISTATS 2010

%Le Song, Arthur Gretton, Carlos Guestrin
%11/09/2010


clear all
close all


%Graph: directions are always from lower to higher cardinality

%     X1
%    /  \
%   X2   X3
%  /  \
% X4   X5


%%%% PARAMETERS OF GRAPHICAL MODEL

%Adjacency matrix: each row denotes a node. For a given row, each
%column denotes a potential child node. A "1" in a
%column indicates that this parent-child link is present.


A = [ 0 1 1 0 0;
      0 0 0 1 1;
      0 0 0 0 0;
      0 0 0 0 0;
      0 0 0 0 0];

numNodes = size(A,1);
rootNode = 1;

%root is mixture of Gaussians: means and std. dev. given
rootMeans = [0 2]; 
rootStd   = 0.2;
p1Root = 0.5;

%Intermediate nodes: take mean of parents, add Gaussian noise. For
%a tree, each node has one parent, so it's just the parent value
%plus noise.
middleStd = 0.2;

%leaf is mixture of Gaussians. Mean either  mean of parents with
%p=p1Leaf, zero otherwise
p1Leaf = 0.5
leafStd = 0.2;

%node ordering from root to leaves. When generating samples, use
%this ordering to sample nodes (to ensure parents are sampled
%before their children).
nodeOrder = 1:5;

%Parzen window at the root: kernel width

sigRoot = 0.1;



%%%% PARAMETERS OF SAMPLE

n=2000; %number of samples

sampArr = zeros(n,numNodes); %each column is a different node

observedList = 4;  %row vector: which node(s) are observed: **may be more than one**
%observedList = [3 4 5]
%observedList = 3;

observations = [0]  %array of observed values at each observed node
%observations = [0.2 0.1 0.0]  
%observations = 0.2;


%%%% PARAMETERS OF INFERENCE PROCEDURE

msgPrm.lambda = 0.1;    %regularization 
msgPrm.sig = 0.3;       %kernel width


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Generate a toy example corresponding to graph A



for sampInd = 1:n

  for nodeInd = 1:numNodes

    whichNode = nodeOrder(nodeInd);
  
    parentInd = find(A(:,whichNode) > 0); %indices of parents

    if (length(parentInd) >= 1) % MIDDLE or LEAF: these have parents

      if (length(find(A(whichNode,:)>0)) >= 1) %MIDDLE: number of children >=1
        mu = mean(sampArr(sampInd,parentInd)); %take mean over all
              %parents add noise. HERE ONLY ONE PARENT: the mean
              %operation has no effect.
        sampArr(sampInd,whichNode) = mu + middleStd * randn(); 
      
      else  %LEAF: either mean of parents+noise, or zero mean+noise
        mu = mean( sampArr(sampInd,parentInd));
        c = rand;
        if (c <= p1Leaf)
          sampArr(sampInd,whichNode) = mu + leafStd* randn;   
        else
          sampArr(sampInd,whichNode) = 0 + leafStd * randn;   
        end            
        
      end
      
    else % ROOT: the node has no parents
      c = rand; 
      if (c <= p1Root )
        sampArr(sampInd,whichNode) = rootMeans(1) + rootStd * randn;
      else
        sampArr(sampInd,whichNode) = rootMeans(2) + rootStd * randn;
      end
    end
    
    
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot the data

for i=1:numNodes
  subplot(3,2,i)
  hist(sampArr(:,i)); title(strcat('Node ',num2str(i)));
end

%pause;
%close

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load data into array for inference algorithm

%In the following, we refer to the graphical model s->t->u

%The Gram matrices for the inference are cached in Karr,
%of size numNodes*numNodes.

%Each row corresponding to a node t has n_children matrices K^(u)_t for each child node
%u (u is column index). These contain the kernel K between the child (u,t) and parent (t,s)
%samples of x_t. If the node is a leaf, with no children, then the node
%contains the Gram matrix with itself, which is stored in entry (t,t).

%Each row corresponding to a node t contains the gram matrix of its PARENT s, based on
%the samples shared by nodes t and s (s is a column index). This is used to compute the
%message from the node t to its parent s. 


for nodeInd = 1:numNodes

  %Compute K_t^(u), Gram matrices of observations wrt parent and
  %observations wrt each of the children. For this toy example, there
  %is a single common sample for both, but this will not always be
  %true in practice. 
  %WARNING: we assume only one parent. For multiple parents, this
  %will fail (not an issue for trees).
  %Also store observations at all leaves in leafArr, to compute messages from leaves.
  for childInd = find(A(nodeInd,:)>0)

    Karr(nodeInd,childInd) = {rbf_dot( sampArr(:,nodeInd), ...
                                             sampArr(:,nodeInd), msgPrm.sig)}; 
  
  end

  %If we are at a leaf, compute the Gram matrix of the leaf sample
  %wrt itself, and store the leaf observations
  if isempty(find(A(nodeInd,:)>0))
    Karr(nodeInd,nodeInd) = {rbf_dot( sampArr(:,nodeInd), ...
                                             sampArr(:,nodeInd), msgPrm.sig)}; 
    leafArr(nodeInd) = {sampArr(:,nodeInd)};
  end
  

  %Compute K_s, the Gram matrix for the parent, based on the
  %samples the parent shares with the present node t. In this toy
  %example, the parent samples are again identical for each of the children
  for parentInd = find(A(:,nodeInd) > 0)
    Karr(nodeInd,parentInd) = {rbf_dot( sampArr(:,parentInd), ...
                                             sampArr(:,parentInd), msgPrm.sig)}; 
  end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Run the inference algorithm

betaArr =  treeInference(A,rootNode,Karr,leafArr,observedList,observations,msgPrm)




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RESULTS PLOTS
%ground truth check: restrict sample to that for which the observed node falls in a
%given interval. This is an approximation to conditioning on a
%specific leaf value (instead we condition on a bin around this value).

%WARNING: ground truth check only works for when a single node is
%observed, AND the sample from the whole graph is seen jointly.
%This is because we compute the conditional distribution empirically.


%%%%%%%%%% root belief  via empirical conditioning

thresh = 0.01; %width of conditioning interval around leaf observation

condIndex = find( (sampArr(:,observedList) > observations-thresh) &...
      (sampArr(:,observedList) < observations+thresh ) );
sprintf('Number of points satisfying conditioning: %i',length(condIndex))

axisBelief = linspace(-5,5,200)';
beliefRootEmp = rbf_dot(axisBelief,sampArr(condIndex,rootNode),sigRoot);
beliefRootEmp = sum(beliefRootEmp');


%%%%%%%%%% root belief via kernel inference algorithm

%First compute unconditional marginal (rootMarginal) using a Parzen
%window estimate
rootMarginal = rbf_dot(axisBelief,sampArr(:,rootNode),sigRoot);
rootMarginal = sum(rootMarginal,2);


%Multiply marginal by product of incoming messages to get condRootMarginal
%NOTE: for the demo, assume a single sample common to all incoming messages, not
%different sample per message (which the inference algorithm implements).

condRootMarginal = rootMarginal;
[A,prunedList] = pruneTree(A,observedList); %Prune children of root
                              %which are leaves with no observations

for childInd = find(A(rootNode,:)>0)
  
 condRootMarginal  = condRootMarginal .* ( rbf_dot(axisBelief,sampArr(:,rootNode),msgPrm.sig)...
                *betaArr{childInd});
  
end

figure
plot(axisBelief,beliefRootEmp/unique(max(beliefRootEmp)));
hold on
plot(axisBelief,condRootMarginal/unique(max(condRootMarginal)),'r');
plot(axisBelief,rootMarginal/unique(max(rootMarginal)),'g');
hold off



legend('emp. cond. root','cond. root','uncond. root','Location','NorthWest')


















%TODO: check that the program runs even when different subsamples
%of different size are used for different node pairs.

%Extension 1: propagate messages down the tree, to get
%marginals at all nodes and not just root.

%Extension 2: junction tree algorithm.

%Extension 3: can we do inference when we never make direct
%observations of the hidden nodes (but they might have nontrivial
%tree structure)?