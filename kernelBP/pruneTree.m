%Prune branches corresponding to unobserved leaves

%Arthur Gretton
%12/09/10

%repeat: if a node unobserved AND is a leaf AND  has a link to a
%parent, cut the link to its parent(s). Iterate until no more
%unobserved leaves. Note: multiple parents are accounted for.

function [A,prunedList] = pruneTree(A,observedList)

numNodes = size(A,1);  


prunedList = [];

numCuts = 1;
while numCuts > 0
  numCuts = 0;
  for nodeInd = 1:numNodes
    if isempty(intersect(nodeInd,observedList)) & ...
          (length(find(A(nodeInd,:)>0)) == 0) & ...
          (length(find(A(:,nodeInd)>0)) >= 1)
      A(:,nodeInd) = zeros(numNodes,1);
      prunedList = [prunedList nodeInd];
      numCuts = numCuts+1;
    end
  end
end
