%Generates a loopy graph, and run kernel loopy BP 

%Demo code for kernel belief propagation
%Le Song, Arthur Gretton, Danny Bickson, Yucheng Low, Carlos Guestrin
%Kernel Belief Propagation
%AISTATS 2011


%Date of last change
%25/09/2010


clear all
close all

%Seeds for random numbers
rand('seed',4);
randn('seed', 4);


a = imread ('test.pgm');
a = imresize (a, 1/30);
[m, n] = size(a);

obs = []; % observed values
img = []; % corresponding true image pixels
unobs_RD = [];
unobs_LU = [];


numb_train_im = 1;
for iter = 1:numb_train_im

    im = double(a);

    img = [img; double(a(:))];


%     img_obs = reshape (obs, m, n);
%     img0 = img_obs;
%     img0(img0<0) = 0;
%     img0(img0>255) = 255;



    a1 = im (1:end-1, :);
    a2 = im (:, 1:end-1);
    unobs_LU = [unobs_LU; a1(:); a2(:)];

    a1 = im (2:end, :);
    a2 = im (:, 2:end);
    unobs_RD = [unobs_RD; a1(:); a2(:)];

end

sigma = 5;
obs = img(:) + sigma * randn(m*n*numb_train_im, 1);



% find kernel bandwidth using median trick; 
tmpidx = randperm(length(img)); 
tmpno = min([3000,length(img)]); 
dismat1 = pdist(img(tmpidx(1:tmpno))); 
sigm_true = 0.5/median(dismat1);   
%sigm_true = 0.05;
K_true = rbf_dot( img, img, sigm_true);


tmpidx = randperm(length(obs)); 
tmpno = min([3000,length(obs)]); 
dismat2 = pdist(obs(tmpidx(1:tmpno)));
sigm_obs = 1/median(dismat2);
%sigm_obs = 0.05;
K_obs = rbf_dot( obs, obs, sigm_obs);
 



%message that is going to be passed to the LU (1) or RD(2)
%LU
K_unary2pairw{2} = rbf_dot(unobs_LU , img, sigm_true);
%RD
K_unary2pairw{1} = rbf_dot(unobs_RD , img, sigm_true);

K_pairwLU = rbf_dot( unobs_LU, unobs_LU, sigm_true);
K_pairwRD = rbf_dot( unobs_RD, unobs_RD, sigm_true);

%LU2RD
K_pairw{1}{2} = rbf_dot( unobs_RD, unobs_LU, sigm_true);
%RD2LU
K_pairw{2}{1}= rbf_dot( unobs_LU, unobs_RD, sigm_true);

K_pairw{1}{1}= K_pairwLU;
K_pairw{2}{2}= K_pairwRD;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Run the inference algorithm

msgPrm.lambda = 0.1;
msgPrm.numIter = 10;


obs_train = obs;

img_obs_test = obs; %double(a(:)) + sigma * randn(m*n, 1);
img_obs_test = reshape (img_obs_test, m, n);


%obs_train observed values for the current image that we make inference for
%img_obs_test all the observed values we've seen during testing
tic
[betaArrFromObs, betaArr] =  loopyInference(K_true, K_obs, K_unary2pairw, K_pairwLU, K_pairwRD, K_pairw, obs_train, img_obs_test, m, n, sigm_true, sigm_obs, msgPrm);
toc






%find max marginal for every node

axisBelief = linspace(1,255,255)';

opp_neigh_ind = [3, 4, 1, 2];
neigh_ind = [-1 0; 0 1; 1 0; 0 -1];


img_new = zeros (m, n);

%LU
K_pairw2unary{2} = rbf_dot(img, unobs_LU , sigm_true);
%RD
K_pairw2unary{1} = rbf_dot(img, unobs_RD, sigm_true);


nodeMarginal = rbf_dot(axisBelief, img, sigm_true);
%nodeMarginal = sum(nodeMarginal,2);


for i= 2:m+1
    for j = 2:n+1


     %  condNodeMarginal = nodeMarginal .* (rbf_dot(axisBelief, img, sigm_true) * betaArrFromObs{i}{j});
      
    
     bettar = (K_true * betaArrFromObs{i}{j});

     
       
       for nghbrInd = 1:4
  
             neigh_i = i + neigh_ind (nghbrInd, 1);
             neigh_j = j + neigh_ind (nghbrInd, 2);
               
             if (neigh_i > 1 && neigh_i < m+2 && neigh_j > 1 && neigh_j < n+2)
             
                 if (nghbrInd == 2 || nghbrInd == 3) 
                    K = K_pairw2unary{2}; %RD
                 else
                    K = K_pairw2unary{1}; %LU
                 end
                
                 bettar  = bettar .* (K ...
                  *betaArr{neigh_i}{neigh_j}{opp_neigh_ind(nghbrInd)});
                % condNodeMarginal  = condNodeMarginal .* (rbf_dot(axisBelief, img, sigm_true)...
                %*betaArr{neigh_i}{neigh_j}{opp_neigh_ind(nghbrInd)});
             end
       end
       
       
%        max_el = max (bettar, [], 2);
%                 
%        bettar = bettar - repmat (max_el, 1, size ( bettar, 2));
%        bettar = log(sum(exp(bettar), 2));
%        bettar = bettar + max_el; 
%        
       res = nodeMarginal * (bettar);

       
       [c, ind] = max (res);
       plot (axisBelief, res);
       img_new(i-1, j-1) = ind;  
       
    end
end

figure(1);
imagesc (img_new);
colorbar;
% figure (2);
% imagesc (img0);
% colorbar;









