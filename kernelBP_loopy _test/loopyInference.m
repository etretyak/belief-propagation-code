%Loopy belief propagation via nonparametric kernel message passing


%Implements the algorithm in the paper:
%Le Song, Arthur Gretton, Danny Bickson, Yucheng Low, Carlos Guestrin
%Kernel Belief Propagation
%AISTATS 2011

%Date of last change:
%25/09/2010

%INPUTS:

% Am: matrix encoding directed edges between nodes. Represents the
%    moralized (if converted from directed) undirected graph.
% Karr: matrix containing Gram matrices for each node
% obsTrainArr: training data at observed nodes
% n: number of training samples per node
% observedList: list of observed leaf nodes
% observations: observations at the observed leaf nodes
% msgPrm: parameters for the nonparametric inference procedure
%         (regularization, kernel size, number of iterations of
%         loopy BP)

%OUTPUTS:

%betaArr: array of beta values encoding the messages passed from each
%node to all its neighbours. betaArr is a cell array of cell
%arrays: each entry is a node, for which each subentry is a vector
%of betas corresponding to the outgoing messages from that node.

function [betaArrFromObs, betaArr] = loopyInference(K_true, K_obs, K_unary2pairw, K_pairwLU, K_pairwRD, K_pairw, obs_train, img_obs, m, n, sigm_true, sigm_obs, msgPrm)

numb_el = n*m;  
img_obs_pad = padarray (img_obs, [1 1]);


for i = 2:m+1
    for j = 2:n+1

        Kt = K_obs;
        Ks = K_true;
        I = eye(size(Kt));

        kt = rbf_dot( obs_train, img_obs_pad(i, j) , sigm_obs);

        betaArrFromObs{i}{j} =  ((Kt + msgPrm.lambda*I)*(Ks + msgPrm.lambda*I)) \kt ;
        %normalize for numerical stability:
        betaArrFromObs{i}{j} = betaArrFromObs{i}{j}/...
        norm(betaArrFromObs{i}{j});
        %betaArrFromObs{i}{j}(betaArrFromObs{i}{j} == 0) = 1e-7;
    end
end



%Initialize the BETAS to be constant vectors with unit norm.

el_ext = size (K_pairwLU, 1);

for i = 1:m+2
  for j = 1:n+2
      for neigh_ind = 1:4
         betaArr{i}{j}{neigh_ind} = ones(el_ext,1)/sqrt(el_ext); 
      end
  end
end


%Precompute the regularized inverse Gram matrices

disp('** Precomputing regularized inverse Gram matrices **')
%for nodeInd = setdiff(1:numNodes,observedList)
%   KarrInvReg{nodeInd} = inv( Karr{nodeInd}  + msgPrm.lambda*eye(n));
%end
    

%Compute INTERNAL messages
%Here the schedule is to iterate over nodes. 


opp_neigh_ind = [3, 4, 1, 2];
neigh_ind = [-1 0; 0 1; 1 0; 0 -1];


K_true_inv{1} = inv( K_pairwLU  + msgPrm.lambda*eye(el_ext));
K_true_inv{2} = inv( K_pairwRD  + msgPrm.lambda*eye(el_ext));


for whichLoop = 1:msgPrm.numIter
  
  fprintf('BP iteration: %i\n',whichLoop);
  
  %iterate over all unobserved nodes
  for i = 2:m+1
    for j = 2:n+1
     
    
        for outMessageInd = 1:4
        %compute incoming message from all nodes besides the one which will
        %receive the outgoing message
      
        
            if (outMessageInd == 1 || outMessageInd == 4)
                curNodeType = 'LU';
                curNodeTypeID = 2;
                
            else
                curNodeType = 'RD';
                curNodeTypeID = 1;
            end
            
            Ktu_Beta = [];
            Ktu_Beta(1:el_ext, 1) = K_unary2pairw{curNodeTypeID} * betaArrFromObs{i}{j}; %ones(numb_el,1);  %The vector K * \beta
     
                    
            for inMessageInd = setdiff([1:4],outMessageInd)
    
                if (opp_neigh_ind(inMessageInd) == 1 || opp_neigh_ind(inMessageInd) == 4)
               
                    inNodeType = 'LU';
                    inNodeTypeID = 2;
                else
                    inNodeType = 'RD';
                    inNodeTypeID = 1;             
                end
                    
                inMessage_i = i + neigh_ind (inMessageInd, 1);
                inMessage_j = j + neigh_ind (inMessageInd, 2);
                
                if (inMessage_i > 1 && inMessage_i < m+2 && inMessage_j > 1 && inMessage_j < n+2)
                    Ktu_Beta(:, end+1) = (K_pairw{inNodeTypeID}{curNodeTypeID} *...
                                   betaArr{inMessage_i}{inMessage_j}{opp_neigh_ind(inMessageInd)});
                        
                end
                           
                               
                
            end
      
            
            max_el = max (Ktu_Beta, [], 2);
                
            Ktu_Beta = Ktu_Beta - repmat (max_el, 1, size (Ktu_Beta, 2));
            Ktu_Beta = log(sum(exp(Ktu_Beta), 2));
            Ktu_Beta = Ktu_Beta + max_el; 
            
            if (opp_neigh_ind(outMessageInd) == 1 || opp_neigh_ind(outMessageInd) == 4)
               nodeTypeID = 2;
            else
               nodeTypeID = 1;             
            end

            
             betaArr{i}{j}{outMessageInd} = K_true_inv{nodeTypeID} * Ktu_Beta ;
              %for numerical stability: rescale betas to have unit norm
             if (norm(betaArr{i}{j}{outMessageInd}) ~= 0)
              betaArr{i}{j}{outMessageInd} =betaArr{i}{j}{outMessageInd}/...
                norm(betaArr{i}{j}{outMessageInd});
             end
            % betaArr{i}{j}{outMessageInd}(betaArr{i}{j}{outMessageInd} == 0) = 1e-7;
        %    display (i);
        %    display (j);
            plot (1:el_ext, betaArr{i}{j}{outMessageInd});
           % pause;
       end
    end
  end  
end

