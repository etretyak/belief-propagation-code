% addpath('/media/d/workspace/common/matlab');
% addpath('itpp-4.0.7/extras/');
addpath('../common/');
addpath('uLSIF/'); 

prefix = '../../data/make3ddata/';
totalpart = 5;
totalno = 274; 
row = 107;
col = 86;
p = 273;

skipno = row*col; 

trialno = 4; 
bias = 1e-4;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% first pass of the data: do log10 transformation on y, standardize x; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (1)
    
    image_count = 0;     
    k = 0; 
    x = zeros(p, skipno*totalno);
    y = zeros(1, skipno*totalno);
    for i = 1:5
        load([prefix, 'part', int2str(i), '.mat']);
        tmp_count = length(part);
        image_count = image_count + tmp_count; 
        for j = 1:tmp_count
            k = k + 1; 
            fprintf(1, '--part %d and image %d\n', i, j); 
            x(:,(k-1)*skipno+(1:skipno)) = ...
                reshape(part{j}.x, [row*col, p])';
            y(:,(k-1)*skipno+(1:skipno)) = ...
                log10(reshape(part{j}.y, [1, row*col]) + bias);
        end
        clear part; 
    end

%     [y, meany, stdy] = standardize(y);
    meany = mean(y); 
    stdy = std(y);     
    maxy = max(y); 
    miny = min(y); 
    m = length(y); 
    
    meanx = mean(x, 2);
    stdx = std(x, 0, 2);
    for i = 1:size(x,1)-1
        fprintf(1, '--standardizing row %d of x\n', i);         
        x(i,:) = (x(i,:) - meanx(i)) ./ stdx(i);         
    end    
end

x0 = x; 
y0 = y; 
totalno0 = 274; 

errmat = zeros(1,totalno0); 
errmat0 = zeros(1,totalno0);
errmat1 = zeros(1,totalno0);

for k = 1:totalno0
    
    fprintf(1, '--preprocessing testing image %d of %d\n', k, totalno0); 

    totalno = totalno0;    
    idxte = (k-1)*skipno + (1:skipno); 
    idxtr = setdiff(1:(skipno*totalno), idxte);
    xte = x0(:, idxte);
    x = x0(:, idxtr);
    yte = y0(:, idxte);
    y = y0(:, idxtr);
    totalno = totalno - 1;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Incomplete cholesky decomposition of y; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % find kernel bandwidth; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % step = fix(m / 10000); 
    step = fix(m / 3000); 
    dismat = pdist(y(1:step:end)').^2; 
    s = 1/median(dismat);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % decompostion of kernel; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fno = 82;
    expno = 1; 
    [fy, A, I] = incomplete_cholRBF(y, s, expno, fno);
    fno = length(I); 
    ybasis = y(:, I); 
    fybasis = fy(:, I); 

    fmu = mean(fy,2); 

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % decompostion of kernel on test points;  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    testmax = 82; 
    testy = log10((0.5:0.1:testmax)+bias); 
    testno = length(testy); 
    % testy = linspace(miny, maxy, testno); 
    ftesty = incomplete_cholRBF_test(testy, ybasis, s, expno);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % decomposition of kernel to power 4; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fno5 = 200; 
    expno = 4; 
    [fy5, A5, I5] = incomplete_cholRBF(y, s, expno, fno5); 
    fno5 = length(I5); 
    ybasis5 = y(:, I5); 
    fybasis5 = fy5(:, I5); 

    cfybasis = fy(:, I5); 
    cfybasis5 = fy5(:, I); 

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute operators; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    iCxx_x = cell(1,row); 
    Cfyfy = cell(1, row);
    iCfyfy = cell(1, row); 
    Cfyx = cell(1, row); 
    Cyx = cell(1, row); 
    for i = 1:row
    %     fprintf(1, '--computing Cfyfy{%d}\n', i); 

        lambda = 1e-3;    
    %     lambda = 1e-2;    
        iCxx_x{i} = ((x(:,i:row:end)*x(:,i:row:end)')./ (col*totalno) + lambda*eye(p)) \ x(:,i:row:end); 
        Cfyfy{i} = fy(:,i:row:end) * fy(:,i:row:end)' ./ (col*totalno);
        lambda = 1e-1;
        iCfyfy{i} = inv(Cfyfy{i} + lambda*eye(fno)); 
        Cfyx{i} = fy(:,i:row:end) * x(:,i:row:end)' ./ (col*totalno); 
        Cyx{i} = y(:,i:row:end) * x(:,i:row:end)' ./ (col*totalno); 
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % conditional embedding operator between left and right pixels depth; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    pUud = cell(1, row); 
    pUdu = cell(1, row); 
    pUlr = cell(1, row);
    pUrl = cell(1, row); 
    
    GMMPlr = cell(1, row);
    GMMPud = cell(1, row);  
    
    sigma_list = 1; 
        
    for i = 1:row
        fprintf(1, '--computing operators pUrl{%d}\n', i);     

        % left to right, right to left; 
        lidx = repmat(i:row:(row*(col-1)), 1, totalno) ...
            + kron(skipno*(0:totalno-1), ones(1, col-1)); 
        ridx = repmat((row+i):row:skipno, 1, totalno) ...
            + kron(skipno*(0:totalno-1), ones(1, col-1)); 
        tClr = fy(:,lidx) * fy5(:,ridx)';
        tCrl = fy(:,ridx) * fy5(:,lidx)';
        mlr = length(lidx);
        mrl = length(ridx);
        tClr = tClr ./ mlr; 
        tCrl = tCrl ./ mrl; 

        pUlr{i} = iCfyfy{i} * tClr * A5'; 
        pUrl{i} = iCfyfy{i} * tCrl * A5'; 

        % mixture of Gaussians for left-right edge potential; 
        if (i == 1) % choose a single bandwidth for all rows; 
            yy = [y(:,lidx); y(:,ridx)]; 
            tmpidx = randperm(length(lidx)); 
            tmpno = min([3000,length(lidx)]);     
            dismat3 = pdist(yy(:,tmpidx(1:tmpno))'); % no .^2; 
            sigma_list = median(dismat3) ./ sqrt(2);
        end
        cno = 100;     
        [ignore, ignore, Iyy] = incomplete_cholRBF(yy, 1/(2*sigma_list.^2), 1, cno, 1e-6);
        cno = length(Iyy); 
        
        [x_ce, alphah, sigma_chosen] = uLSIF( ...
            [y(:,lidx(randperm(length(lidx)))); y(:,ridx)], ...
            [y(:,lidx); y(:,ridx)], ...
            [], sigma_list, [], cno, 0, Iyy); 
        GMMPlr{i}.x_ce = x_ce; 
        GMMPlr{i}.alpha = alphah'; 
        GMMPlr{i}.sigma = sigma_chosen * ones(1,cno); 
                        
        if (i < row) % up to down; 
            uidx = i:row:(skipno*totalno); 
            didx = (i+1):row:(skipno*totalno); 
            mud = length(uidx); 
            tCud = (fy(:,didx) * fy5(:,uidx)') ./ mud; 
            pUud{i+1} = iCfyfy{i+1} * tCud * A5'; 

            % mixture of Gaussians for up to down edge potential; 
%             yy = [y(:,uidx); y(:,didx)]; 
%             tmpidx = randperm(length(uidx)); 
%             tmpno = min([3000,length(uidx)]);     
%             dismat3 = pdist(yy(:,tmpidx(1:tmpno))'); % no .^2; 
%             sigma_list = median(dismat3) ./ sqrt(2);
            cno = 100;     
            [ignore, ignore, Iyy] = incomplete_cholRBF(yy, 1/(2*sigma_list.^2), 1, cno, 1e-6);
            cno = length(Iyy);             
            
            [x_ce, alphah, sigma_chosen] = uLSIF( ...
                [y(:,uidx(randperm(length(uidx)))); y(:,didx)], ...
                [y(:,uidx); y(:,didx)], ...
                [], sigma_list, [], cno, 0, Iyy); 

            GMMPud{i}.x_ce = x_ce; 
            GMMPud{i}.alpha = alphah'; 
            GMMPud{i}.sigma = sigma_chosen * ones(1,cno);                         
        end

        if (i > 1) % down to up;         
            uidx = (i-1):row:(skipno*totalno);
            didx = i:row:(skipno*totalno);
            mdu = length(uidx);
            tCdu = (fy(:,uidx) * fy5(:,didx)') ./ mdu; 
            pUdu{i-1} = iCfyfy{i-1} * tCdu * A5'; 
                        
        end

    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute additional prior; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    prior = zeros(fno, row); 
    for i = 1:totalno*col
        prior = prior + fy(:, (i-1)*row + (1:row)); 
    end
    prior = prior ./ (totalno*col); 
    prior = repmat(prior, 1, col);
       
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % linear vs nonlinear prediction; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    predy = zeros(1, skipno*totalno); 
    for i = 1:row
        predy(i:row:end) = Cyx{i} * iCxx_x{i};
    end
    mean(abs(predy - y))

    lambda = 1e0; 
    predfy = zeros(fno, skipno*totalno);     
    for i = 1:row
        predfy(:, i:row:end) = ((Cfyfy{i} + lambda*eye(fno)) \ Cfyx{i}) * iCxx_x{i}; 
    end
    [ignore, maxidx] = max(ftesty' * predfy, [], 1); 
    testy_max = testy(maxidx); 
    mean(abs(testy_max - y))

    % errmat0 = zeros(1,totalno); 
    % errmat1 = zeros(1,totalno); 
    % for i = 1:totalno
    %     truey = y((i-1)*skipno+(1:skipno)); 
    %     errmat0(i) = mean(abs(testy_max((i-1)*skipno+(1:skipno)) - truey)); 
    %     errmat1(i) = mean(abs(predy((i-1)*skipno+(1:skipno)) - truey)); 
    % end

    % [errmat0(:), errmat1(:)]         

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % perform inference;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    lambda = 1e-3; % 1e-4; 
    iCxx_xte = cell(1,row);
    for i = 1:row
        iCxx_xte{i} = ((x(:,i:row:end)*x(:,i:row:end)')./ (col*totalno) + lambda*eye(p)) \ xte(:,i:row:end);         
    end
    
    % convert P(x_s, o_s) \propto P(x_s | o_s) into mixture of Gaussians; 
    sigma_chosen = sqrt(1/(2*s)); % scale parameter to standard deviation; 
    x_ce = ybasis; 
    like = cell(1,row); 
    for i = 1:row
        Kce = fybasis' * fybasis;  
        Kh = fybasis' * Cfyx{i} * iCxx_xte{i}; 
        alphat = (Kce + 1e-4*eye(fno)) \ Kh; 
        alphah = max(0,alphat);        
        like{i}.x_ce = x_ce;
        like{i}.alpha = alphah'; 
        like{i}.sigma = sigma_chosen * ones(1,fno); 
    end    

    predy_k = zeros(1,skipno); 
    for i = 1:row    
        predy_k(i:row:end) = Cyx{i} * iCxx_xte{i};
    end

    lambda = 1e0; % 1e0; 
    predfy_k = zeros(fno,skipno);
    for i = 1:row
        predfy_k(:,i:row:skipno) = ((Cfyfy{i} + lambda*eye(fno)) \ Cfyx{i}) * iCxx_xte{i};     
    end

    % predfy_k = predfy(:, (k-1)*skipno+(1:skipno));
    prod_msg0 = cfybasis' * predfy_k;
    belief0 = ftesty' * predfy_k; 
    belief0 = belief0 .* (ftesty' * prior); 

    lambda = 1e-6; 
    m0 = ((ftesty * ftesty' + lambda*eye(fno)) \ ftesty) * ones(testno, 1);
    m0 = m0 ./ sqrt(sum(m0.^2)); 
    m1 = repmat(m0, 1, skipno); 
    m2 = m1; 
    m3 = m1; 
    m4 = m1; 
    belief = ones(testno, row*col) ./ testno; 
    lfeat_m = zeros(fno5, 4);  
    
    % truey = y((k-1)*skipno+(1:skipno));      
    truey = yte;

    isize = [row, col];     
    itsave([prefix, 'KBP_fold', int2str(k), '_part', '0', '.it'], ...
        isize, m0, predy_k, predfy_k, prod_msg0, belief0, testy, truey, cfybasis, ftesty);

    for i = 1:row
        ipUud = pUud{i};
        ipUdu = pUdu{i};
        ipUlr = pUlr{i};        
        ipUrl = pUrl{i}; 
        itsave([prefix, 'KBP_fold', int2str(k), '_part', int2str(i), '.it'], ...
            ipUud, ipUdu, ipUlr, ipUrl);         
                
        like_ce = like{i}.x_ce;
        like_alpha = like{i}.alpha;
        like_sigma = like{i}.sigma;

        lr_edge_ce = GMMPlr{i}.x_ce;
        lr_edge_alpha = GMMPlr{i}.alpha;
        lr_edge_sigma = GMMPlr{i}.sigma;
        
        if (i < row)
            ud_edge_ce = GMMPud{i}.x_ce;
            ud_edge_alpha = GMMPud{i}.alpha;
            ud_edge_sigma = GMMPud{i}.sigma;
        else
            ud_edge_ce = GMMPud{i-1}.x_ce;
            ud_edge_alpha = GMMPud{i-1}.alpha;
            ud_edge_sigma = GMMPud{i-1}.sigma;
        end
        
        itsave([prefix, 'GMM_fold', int2str(k), '_part', int2str(i), '.it'], ...
            like_ce, like_alpha, like_sigma, ...
            lr_edge_ce, lr_edge_alpha, lr_edge_sigma, ...
            ud_edge_ce, ud_edge_alpha, ud_edge_sigma );
    end
        
end

    
