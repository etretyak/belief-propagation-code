clear; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prepare denoising data to run in graphlab; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% addpath('/media/d/workspace/common/matlab');
% addpath('itpp-4.0.7/extras/');
addpath('uLSIF/'); 
% addpath('LSCDE/');

prefix = 'images/';
for src = [10, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250]
    %a = imread([prefix, 'source', int2str(src), '.pgm']);
    a = imread ([prefix, 'source_img' '.pgm']);
    a = imresize(a, [100, 100], 'nearest');
    [m, n] = size(a); 

    ua = unique(a)
    l = length(ua); 

    % observation noise standard deviation; 
    sigma = 30;
    
    randn('state', src); 
    rand('state', src);
    
    % flattened true image: 
    img1 = double(a(:)'); 
    % and flattened transposition of the true image; 
    aa = a';
    img2 = double(aa(:)'); 
    
    % noisy observation for training; 
    obs1 = double(a(:)') + sigma * randn(1, m*n);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Processing for original feature space of 
    % the true image; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

    % find kernel bandwidth using median trick; 
    tmpidx = randperm(length(img1)); 
    tmpno = min([3000,length(img1)]); 
    dismat1 = pdist(img1(tmpidx(1:tmpno))').^2; 
    s1 = 0.5/median(dismat1);         
    % perform incomplete cholesky decomposition with Gaussian RBF kernel; 
    fno = 100; 
    expno = 1; 
    [fimg1, A1, I1] = incomplete_cholRBF(img1, s1, expno, fno, 1e-4);
    fimg2 = incomplete_cholRBF_test(img2, img1(:,I1), s1, expno);
    
    % test points used in decoding the belief; 
    test = double(ua(:)');
    testno = length(test); 
    ftest = incomplete_cholRBF_test(test, img1(:,I1), s1, expno); 
    
    % parzen window as node potential 
    fmu = mean(fimg1, 2); 
    fno1 = length(I1);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Processing for tensor product feature space
    % of the true image; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    
    expno = 4; 
    [fimg5, A5, I5] = incomplete_cholRBF(img1, s1, expno, fno, 1e-4); 
    fno5 = length(I5); 
    fimg6 = incomplete_cholRBF_test(img2, img1(:,I5), s1, expno);    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Processing for original feature space
    % of the noisy observations; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    tmpidx = randperm(length(obs1)); 
    tmpno = min([3000,length(obs1)]); 
    dismat2 = pdist(obs1(tmpidx(1:tmpno))').^2;
    s2 = 1/median(dismat2);
    expno = 1; 
    [fobs1, A2, I2] = incomplete_cholRBF(obs1, s2, expno, fno, 1e-4); 
    fno2 = length(I2);           
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % generate things related to 
    % embedding operators. 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    lfeat = [fimg5(:, 1:end-m), fimg5(:, m+1:end), fimg6(:, 1:end-n), fimg6(:, n+1:end)];
    rfeat = [fimg1(:, m+1:end), fimg1(:, 1:end-m), fimg2(:, n+1:end), fimg2(:, 1:end-n)];   

    % estimate conditional embedding operator for adjacent nodes; 
    lambda = 1e-2;    
    pU = (A5 * lfeat * rfeat') / (rfeat * rfeat' + lambda * eye(fno1)); 
    pU = pU';
    
    % estimate likelihood function;    
    lambda = 1e-2;
    pL = (fobs1 * fimg1') / (fimg1 * fimg1' + lambda * eye(fno1)); 
    pL = pL';

    lambda = 1e-6;
    alpha = (ftest' * ftest + eye(testno) * lambda) \ ones(length(ua), m*n);
    m1 = ftest * alpha; 
    m2 = m1; 
    m3 = m1; 
    m4 = m1; 
    belief = ones(l, m*n) ./ l; 

    isize = [m, n];
    m0 = m1(:,1); 

    tmp_prod_msg = fimg1(:,I5)' * pL; 
    tmp_ftest_pL = ftest' * pL; 
    lfeat_m = zeros(length(I5),4); 
    fimgbasis = fimg1(:,I5); 
    
    itsave([prefix, 'KBP_model', int2str(src), '.it'], pU, pL, fimgbasis, test, ftest, isize, m0, fmu, tmp_prod_msg, tmp_ftest_pL);  
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute potential and likelihood 
    % as mixture of Gaussians; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
    
    % estimate P(x_s, x_t) / (P(x_s) P(x_t)); 
    yy1 = [img1(:, 1:end-m), img1(:, m+1:end), img2(:, 1:end-n), img2(:, n+1:end)];
    yy2 = [img1(:, m+1:end), img1(:, 1:end-m), img2(:, n+1:end), img2(:, 1:end-n)];             
    
    yy = [yy1; yy2]; 
    tmpidx = randperm(length(yy1)); 
    tmpno = min([3000,length(yy1)]);     
    dismat3 = pdist(yy(:,tmpidx(1:tmpno))'); % no .^2; 
    sigma_list = sqrt(0.5) * median(dismat3) ./ sqrt(2);
    
    cno = 100;     
    [ignore, ignore, Iyy] = incomplete_cholRBF(yy, 1/(2*sigma_list.^2), 1, cno, 1e-6);               
    cno = length(Iyy); 
        
    [x_ce, alphah, sigma_chosen] = uLSIF( ...
        [yy1(randperm(length(yy1))); yy2], ...
        [yy1; yy2], ...
        [], sigma_list, [], cno, 0, Iyy);     
    edge_ce = x_ce; % Gaussian center;
    edge_alpha = alphah';  % mixing weights, positive but not necessarily sum to 1;  
    edge_sigma = sigma_chosen * ones(1,cno); % standard deviation; 
    
%     x0 = [0:255]; 
%     y0 = [0:255]; 
%     [gridx, gridy] = meshgrid(x0, y0);
%     Kh = kernel_Gaussian(edge_ce, [gridx(:), gridy(:)]', edge_sigma(1));
%     aKh = edge_alpha * Kh;
%     figure; imagesc(reshape(aKh, [length(y0), length(x0)]));     
        
    % estimate P(o_s, x_s).    
    yy = [img1; obs1];
    tmpidx = randperm(length(img1)); 
    tmpno = min([3000,length(img1)]);         
    dismat4 = pdist(yy(:,tmpidx(1:tmpno))'); % no .^2;  
    sigma_chosen = sqrt(0.5) * median(dismat4) ./ sqrt(2);
    
    cno2 = 100; 
    [ignore, ignore, Iyy] = incomplete_cholRBF(yy, 1/(2*sigma_chosen.^2), 1, cno2, 1e-6);           
    cno2 = length(Iyy);
    
    x_ce = yy(:,Iyy); 
    Kce = kernel_Gaussian(x_ce,x_ce,sigma_chosen);
    Kh = mean(kernel_Gaussian(x_ce,yy,sigma_chosen), 2); 
    alphat = (Kce + 1e-4*eye(cno2)) \ Kh; 
    alphah = max(0,alphat);
    like_ce = x_ce;
    like_alpha = alphah'; 
    like_sigma = sigma_chosen * ones(1, cno2); 
    
%     x0 = [0:255]; 
%     y0 = [-100:400]; 
%     [gridx, gridy] = meshgrid(x0, y0);
%     Kh = kernel_Gaussian(like_ce, [gridx(:), gridy(:)]', like_sigma(1));
%     aKh = like_alpha * Kh;
%     figure; imagesc(flipud(reshape(aKh, [length(y0), length(x0)])));     
    
    
    itsave([prefix, 'GMM_model', int2str(src), '.it'], img1, obs1, test, isize, edge_ce, edge_alpha, edge_sigma, like_ce, like_alpha, like_sigma);      
        
%     return; 
    
%     keyboard;
                        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Generate test noisy observations and 
    % processing the feature matrix; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for ri = 1:50
        fprintf(1, '--randomization %d\n', ri);   

        obs2 = double(a(:)') + sigma * randn(1, m*n);    
        fobs2 = incomplete_cholRBF_test(obs2, obs1(:,I2), s2, expno);         
        
        itsave([prefix, 'Test', int2str(src), '_', int2str(ri), '.it'], obs2, fobs2, img1);
    end
end

