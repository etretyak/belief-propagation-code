prefix = 'images/';
a = imread('source_img.pgm');
[m, n] = size(a); 
sigma = 1; 
b = double(a) + sigma * randn(m, n); 

% b(b<0) = 0; 
% b(b>255) = 255; 
% b = uint8(b); 

% imwrite(b, ['img_noise', num2str(sigma), '.pgm'], 'pgm');