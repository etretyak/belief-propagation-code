%Incomplete Cholesky decomposition.

%Compared with Kchol.m, this version is for tensor products of kernels

%Original code from Kernel Methods for Pattern Analysis, 
%Shawe-Taylor and Cristianini.
%All page references in comments are to this book.

%Modifications: Arthur Gretton
% Compute kernel values on the fly, rather than requiring the Gram matrix as input
% Return R2, the coefficients allowing us to construct the feature
%            map of the sample from the feature map of the pivots.
% Take the kernel function as an argument
% When a list of kernel functions is given as an argument, do
% incomplete Cholesky for the tensor product of these kernels.

%04/06/2011


%WARNING: assumes all kernels are one on the diagonal

%Definitions and note: 
%\Phi is a "matrix" where each column is the feature map of an
%input point.
%\Phi_I contains the subset I of the columns of \Phi
%We return the approximation: Phi \approx Phi_I R
%The Gram matrix is approximately Phi^T Phi
% = R2' \Phi_I' Phi_I R2 = R2' Kout R2

%INPUT:
%  X contains a cell array of X_i of size m * di, where m is number of sample points
%  whichKer: cell array of kernel functions to use (one for each X_i)
%  kParam = kernel parameters. For instance kParam.sig is kernel size
%  eta = residual precision

%OUTPUT:
%  Kout = matrix of inner products between pivots
%  I = index set of pivots. There are l pivots
%  R: by eq. (B), top of p, 126: K\approx R'*R
%      The l rows of R correspond to the pivot indices
%      The m columns of R correspond to the sample indices
%  R2: \Phi \approx \Phi_I * R2 where  R2 has size l*m




function [Kout, I, R,R2] = KcholTensor(X,whichKer,kParam,eta)


m=size(X{1},1); %get the sample size from one of the X_i

j = 0; 
R = zeros(m,m); 
d =  ones(m,1); %diagonal of RBF kernel is initially ones. Assume
                %all kernels have ones on the diagonal
[a,I(j+1)] = max(d); 

Karr=zeros(m,m);

while a > eta
    j = j + 1; 
    nu(j) = sqrt(a); 
    for i = 1:m
        Kji = 1;
        for kerInd = 1:length(whichKer)

            Kji = Kji *...
                  feval(whichKer{kerInd},X{kerInd}(I(j),:),X{kerInd}(i,:),kParam);
        end

        Karr(j,i) = Kji;
        R(j,i) = (Kji - R(:,i)'*R(:,I(j)))/nu(j);
        d(i) = d(i) - R(j,i)^2;   %(C) p. 126
    end
    [a,I(j+1)] = max(d); 
    
   
end

I = I(1:end-1);

l = j; R = R(1:l,:); 

R2 = R(:,I) \ R;  %NOTE: we take R at the pivot points when
                     %computing R2



%NOTE: Karr contains the inner products of the pivot points with
%every sample point. We only need the inner products between
%pivots. Thus, we retain only the pivot indices of the Karr
%columns, when returning Kout.

%NOTE 2: the rows of R and R2 correspond to the ordering of the original samples

Kout = Karr(1:j,I);




