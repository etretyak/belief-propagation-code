%Combines multiple incoming messages using an incomplete Cholesky
%representation for computational efficiency.

%Implements the approach of Section 5.3 in the paper:
%Kernel Belief Propagation
%Le Song, Arthur Gretton, Danny Bickson, 
%Yucheng Low, Carlos Guestrin
%AISTATS 2011

%Arthur Gretton

%12/07/2011

%Demonstrated with two edges feeding an internal node, which feeds
%the root: this is the simplest graph allowing all aspects of the
%Cholesky message passing to be shown.
%
%            Q
%            ^
%            |
% hX -> X -> Z <- Y <- hY


%hX and hY  are parents of X and Y, representing which component of
%the mixture generated the variable



clear all
close all

%Seeds for random numbers
%rand('seed',4);
%randn('seed', 4);



%Generate data

m = 5000;   %number of samples from each edge


%Each of X,Y is a mixture of Gaussians centred at +1/-1
%Z is a noisy sum of the the two inputs.
%Q is a noisy nonlinear function of Z.

%mixture of Gaussians: means and std. dev. given
nodeMeans = [-1 1]; 
nodeStds   = 0.2;  %Gaussians in mixture model at leaves X,Y
zStd = 0.1;        %Gaussian noise added to  node Z
qStd = 0.1;        %Gaussian noise added to  node Q
mixP = 0.5;        %Mixture model weight for X and Y

%Values of Z on which belief of Z is evaluated
axisBeliefQ = linspace(-5,5,200)';


%Observed X and Y determine the mode of the posterior on Z

obsX = 1;   %new observation of X
obsY = -1;  %new observation of Y




%%%% PARAMETERS OF INFERENCE PROCEDURE

msgPrm.lambda = 0.1;    %regularization
msgPrm.sig = 0.1;       %kernel width
msgPrm.eta = 0.01;     %Precision of incomplete Cholesky

%Parzen window at the root: kernel width
sigRoot = 0.1;


%%%% OPTION OF SWITCHING OFF FULL RANK OR TENSOR COMPARISON

doFullRank = 1;
doTensor = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Generate training observations at nodes X, Y, Z

hX =  (rand(m,1) > 0.5) * (nodeMeans(2)-nodeMeans(1)) + nodeMeans(1);
X = nodeStds*randn(m,1) + hX;

hY =  (rand(m,1) > 0.5) * (nodeMeans(2)-nodeMeans(1)) + nodeMeans(1);
Y = nodeStds*randn(m,1) + hY;

Z = X + Y + zStd*randn(m,1);

Q = cos(Z) + qStd*randn(m,1);





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FULL RANK COMPUTATION OF ROOT BELIEF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if doFullRank
    tic

    %Gram matrices
    Kx = rbf_dot(X,X,msgPrm.sig);
    Ky = rbf_dot(Y,Y,msgPrm.sig);
    Kz = rbf_dot(Z,Z,msgPrm.sig);
    Kq = rbf_dot(Q,Q,msgPrm.sig);

    I = eye(size(Kz));

    %Coeffs of message from each of X and Y
    kx = rbf_dot( X , obsX ,msgPrm.sig );
    betaX =  ((Kx + msgPrm.lambda*I)*(Kz+msgPrm.lambda*I)) \kx ;
    
    ky = rbf_dot( Y , obsY ,msgPrm.sig );
    betaY =  ((Ky + msgPrm.lambda*I)*(Kz+msgPrm.lambda*I)) \ky ;
   
    %Coeffs of message from Z to root Q
    %DEBUG: assume a single Z sample common to all incoming messages, not
    %different sample per message (which would be more
    %general). This is allowed since all training samples were
    %generated simultaneously.
    betaZ = (Kq + msgPrm.lambda*I)\ ((Kz * betaX).*(Kz*betaY));
    
    
    %Root belief
    preQMarginal = rbf_dot(axisBeliefQ,Q,sigRoot);
    qMarginal = sum(preQMarginal,2);
    
    
    %Multiply marginal of Q by product of incoming messages to get
    %conditional at Q.
    condQMarginal  = qMarginal .* ( preQMarginal*betaZ );
    
    
    figure
    hold on
    plot(axisBeliefQ,condQMarginal/unique(max(condQMarginal)),'r');
    plot(axisBeliefQ,qMarginal/unique(max(qMarginal)),'g');
    hold off
    
    legend('conditional','unconditional')
    
    disp('**done (full rank)**')
    toc

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INCOMPLETE CHOLESKY COMPUTATION OF ROOT BELIEF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if doTensor

tic


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%First precompute matrices that depend only on training data
[KcX, IX, RX, R2X] = KcholTensor({X},{@Krbf},msgPrm,msgPrm.eta);
[KcY, IY, RY, R2Y] = KcholTensor({Y},{@Krbf},msgPrm,msgPrm.eta);
[KcZ, IZ, RZ, R2Z] = KcholTensor({Z},{@Krbf},msgPrm,msgPrm.eta);
[KcQ, IQ, RQ, R2Q] = KcholTensor({Q},{@Krbf},msgPrm,msgPrm.eta);

iKcX = inv(KcX);
iKcY = inv(KcY);
iKcZ = inv(KcZ);
iKcQ = inv(KcQ);


%Incomplete Cholesky for tensor features
[KcL, IL, RL, R2L] = KcholTensor({X,Y},{@Krbf,@Krbf},msgPrm,msgPrm.eta);
%Gram matrix between tensor pivots and non-tensor pivots
kzTensor = rbf_dot( Z(IL) , Z(IZ),msgPrm.sig );


%Precomputation for cholesky messages from leaves.
%The cost is O(m*l^2) where l << m

Azx = iKcZ * inv( R2Z*R2Z' + iKcZ) * (R2Z * R2X') *inv( R2X*R2X' + iKcX) ...
      * iKcX;

Azy = iKcZ * inv( R2Z*R2Z' + iKcZ) * (R2Z * R2Y') *inv( R2Y*R2Y' + iKcY) ...
      * iKcY;

%Precomputation for internal message from Z to Q. Again, the cost
%is  O(m*l^2) 

Aqz = iKcQ * inv( R2Q*R2Q' + iKcQ) * (R2Q * R2L')  ;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%INFERENCE STEP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%The following vectors are functions of the new leaf observations.
%IMPORTANT NOTE: the inner products need
%only be computed with respect to the retained points in the
%incomplete Cholesky.

kxI = rbf_dot( X(IX) , obsX ,msgPrm.sig );
kyI = rbf_dot( Y(IY) , obsY ,msgPrm.sig );

%Coefficients of messages from leaves X,Y  to Z
betaX = Azx * kxI;
betaY = Azy * kyI;

%Coefficients of message from Z to Q
betaZ = Aqz * ((kzTensor*betaX).*(kzTensor*betaY))

%Root marginal belief is dcomputed with all samples
qMarginal = sum(  rbf_dot(axisBeliefQ,Q,sigRoot) ,2);

    
%Multiply marginal by product of incoming messages to get
%zMarginalCond. We only compute messages wrt pivot points.
preQMarginal = rbf_dot(axisBeliefQ,Q(IQ),sigRoot);    
condQMarginal  = qMarginal .* ( preQMarginal*betaZ) ;
    
    
hold on
plot(axisBeliefQ,condQMarginal/unique(max(condQMarginal)),'b');
hold off
    
legend('conditional','unconditional','cholesky conditional')
    
disp('**done (cholesky)**')
toc

end %if doTensor

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if doTensor
    disp('DIAGNOSTICS:');
    
    fprintf('Sample size: %i\n',m);
    fprintf('Incomplete Cholesky feature dimension for Z: %i\n',size(IZ,2));



    %Check reconstruction error of incomplete Cholesky on leaves and root for
    %the given precision msgPrm.eta
    fprintf('Tensor Cholesky reconstruction error for Kz.*Ky: %2.2d\n',...
            sum(sum(abs(Kx.*Ky- RL'*RL))) / sum(sum(abs(Kx.*Ky))));
    fprintf('Cholesky reconstruction error for Kz: %2.2d\n',...
            sum(sum(abs(Kz- RZ'*RZ))) / sum(sum(abs(Kz))));


end
