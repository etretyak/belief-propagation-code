%Radial basis funcion kernel

%Arthur Gretton
%04/06/11

%X and Y are assumed to be ROW vectors.

function k = Krbf(X,Y,kParam)



k = exp(-1/kParam.sig* (X - Y)*(X-Y)' );