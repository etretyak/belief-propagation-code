%Generates a loopy graph, and run kernel loopy BP 

%Demo code for kernel belief propagation
%Le Song, Arthur Gretton, Danny Bickson, Yucheng Low, Carlos Guestrin
%Kernel Belief Propagation
%AISTATS 2011


%Date of last change
%25/09/2010


clear all
close all

%Seeds for random numbers
rand('seed',4);
randn('seed', 4);

%Graph: directed edges are always from lower to higher cardinality

%     X1
%    /  \
%   X2   X3
%    \  / \
%     X4  X5



%%%% PARAMETERS OF GRAPHICAL MODEL

%Adjacency matrix: each row denotes a node. For a given row, each
%column denotes a potential child node. A "1" in a
%column indicates this parent-child link is present.


A = [ 0 1 1 0 0; %x1 -> (x2,x3)
      0 0 0 1 0; %x2 -> x4
      0 0 0 1 1; %x3 -> (x4,x5)
      0 0 0 0 0; %x4 has no children
      0 0 0 0 0];%x5 has no children


%Moralized undirected graph
%     X1
%    /  \
%   X2---X3
%    \  / \
%     X4  X5

Am = [0 1 1 0 0; %x1 - (x2,x3)
      1 0 1 1 0; %x2 - (x1,x3,x4)
      1 1 0 1 1; %x3 - (x1,x2,x4,x5)
      0 1 1 0 0; %x4 - (x2,x3)
      0 0 1 0 0];%x5 - x3 


numNodes = size(A,1);
rootNode = 1;

%root is mixture of Gaussians: means and std. dev. given
rootMeans = [0 2]; 
rootStd   = 0.2;
p1Root = 0.5;

%Nodes x2 and x3: take mean of parents, add Gaussian noise
middleStd = 0.2;

%leaf is mixture of Gaussians. Mean either mean of parents with
%p=p1Leaf, zero otherwise
p1Leaf = 0.5;
leafStd = 0.2;

%node ordering from root to leaves. When generating samples, use
%this ordering to sample nodes (to ensure parents are sampled
%before their children).
nodeOrder = 1:5;

%Parzen window at the root: kernel width

sigRoot = 0.1;



%%%% PARAMETERS OF SAMPLE

n=5000; %number of samples

sampArr = zeros(n,numNodes); %each column is a different node

observedList = 4;  %row vector: which node(s) are observed
%observedList = [3 4 5]
%observedList = 3;

observations = 0;  %array of observed values at each observed node
%observations = [0.2 0.1 0.0]  
%observations = 0.2;


%%%% PARAMETERS OF INFERENCE PROCEDURE

msgPrm.lambda = 0.1;    %regularization 
msgPrm.sig = 0.3;       %kernel width

msgPrm.numIter = 30;    %number of iterations of loopy BP



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Generate a toy example corresponding to graph A



for sampInd = 1:n

  for nodeInd = 1:numNodes

    whichNode = nodeOrder(nodeInd);
  
    parentInd = find(A(:,whichNode) > 0); %indices of parents

    if (length(parentInd) >= 1) % MIDDLE or LEAF: these have parents

      if (length(find(A(whichNode,:)>0)) >= 1) %MIDDLE: number of children >=1
        mu = mean(sampArr(sampInd,parentInd)); %take mean over all
                                               %parents add noise
        sampArr(sampInd,whichNode) = mu + middleStd * randn(); 
      
      else  %LEAF: either mean of parents+noise, or zero mean+noise
        mu = mean( sampArr(sampInd,parentInd));
        c = rand();
        if (c <= p1Leaf)
          sampArr(sampInd,whichNode) = mu + leafStd* randn();   
        else
          sampArr(sampInd,whichNode) = 0 + leafStd * randn();   
        end            
        
      end
      
    else % ROOT: the node has no parents
      c = rand(); 
      if (c <= p1Root )
        sampArr(sampInd,whichNode) = rootMeans(1) + rootStd * randn();
      else
        sampArr(sampInd,whichNode) = rootMeans(2) + rootStd * randn();
      end
    end
    
    
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot histograms of the training observations at each node

for i=1:numNodes
  subplot(3,2,i)
  hist(sampArr(:,i)); title(strcat('Node ',num2str(i)));
end

pause(1)
close

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load data into array for inference algorithm

%At each round, the node sends messages to each of its
%neighbours. These messages depend on the incoming messages from
%the remaining neighbours.

%CASE WHERE TRAINING SAMPLE A SIMULTANEOUS DRAW FROM ENTIRE MODEL:
%All the Gram matrices for a given node are identical, and we only need a single Gram
%matrix per node. This is the case here.

%The Gram matrices for the inference are cached in Karr,
%of size numNodes.


disp('** Computing Gram matrices **')
for nodeInd = 1:numNodes

  Karr{nodeInd} = rbf_dot( sampArr(:,nodeInd),sampArr(:,nodeInd), msgPrm.sig);
  
end

%The  training samples are cached for the observed node(s), for use
%with the new (test) observation(s).

for observedInd = observedList
  obsArr{observedInd} = sampArr(:,observedInd);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Run the inference algorithm


tic
betaArr =  loopyInference(Am,Karr,obsArr,n,observedList,observations,msgPrm);
toc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUGGING
%ground truth check: restrict sample to that for which the observed node falls in a
%given interval

%DEBUG: ground truth check only works for when a single node is
%observed, AND the sample from the whole graph is seen jointly.
%This is because we compute the conditioning empirically.

thresh = 0.01; %width of conditioning interval around observation

condIndex = find( (sampArr(:,observedList) > repmat (observations-thresh, n, 1)) &...
      (sampArr(:,observedList) < repmat (observations+thresh, n, 1) ) );

  condIndex = find( (sampArr(:,observedList) > observations-thresh) &...
      (sampArr(:,observedList) < observations+thresh ) );

  
sprintf('Number of points satisfying conditioning: %i',length(condIndex))

%condIndex = sum (condIndex, 2);
%condIndex = condIndex == length (observations);

axisBelief = linspace(-5,5,200)';
beliefRootEmp = rbf_dot(axisBelief,sampArr(condIndex,rootNode),sigRoot);
beliefRootEmp = sum(beliefRootEmp');


%%%%%%%%%% root belief via inference algorithm

%First compute unconditional marginal (rootMarginal)
rootMarginal = rbf_dot(axisBelief,sampArr(:,rootNode),sigRoot);
rootMarginal = sum(rootMarginal,2);


%Multiply marginal by product of incoming messages to get condRootMarginal
%Again, assume a single sample common to all incoming messages, not
%different sample per message

condRootMarginal = rootMarginal;

for nghbrInd = find(A(rootNode,:)>0)
  
 condRootMarginal  = condRootMarginal .* ( rbf_dot(axisBelief,sampArr(:,rootNode),msgPrm.sig)...
                *betaArr{nghbrInd}{rootNode});
  
end

figure
plot(axisBelief,beliefRootEmp/unique(max(beliefRootEmp)));
hold on
plot(axisBelief,condRootMarginal/unique(max(condRootMarginal)),'r');
plot(axisBelief,rootMarginal/unique(max(rootMarginal)),'g');
hold off



legend('emp. cond. root','cond. root','uncond. root','Location','NorthWest')







