%Loopy belief propagation via nonparametric kernel message passing


%Implements the algorithm in the paper:
%Le Song, Arthur Gretton, Danny Bickson, Yucheng Low, Carlos Guestrin
%Kernel Belief Propagation
%AISTATS 2011

%Date of last change:
%25/09/2010

%INPUTS:

% Am: matrix encoding directed edges between nodes. Represents the
%    moralized (if converted from directed) undirected graph.
% Karr: matrix containing Gram matrices for each node
% obsTrainArr: training data at observed nodes
% n: number of training samples per node
% observedList: list of observed leaf nodes
% observations: observations at the observed leaf nodes
% msgPrm: parameters for the nonparametric inference procedure
%         (regularization, kernel size, number of iterations of
%         loopy BP)

%OUTPUTS:

%betaArr: array of beta values encoding the messages passed from each
%node to all its neighbours. betaArr is a cell array of cell
%arrays: each entry is a node, for which each subentry is a vector
%of betas corresponding to the outgoing messages from that node.

function [betaArr] = loopyInference(Am,Karr,obsTrainArr,n,observedList,observations,msgPrm)

numNodes = size(Am,1);  
  


%Compute messages from OBSERVED LEAVES
%There can be multiple neighbours of an observed
%leaf. Index t is the leaf, index s is the hidden node receiving
%the leaf message.

%The message passed from oberved leaf x_t to node X_s is
%p(x_t|X_s), in accordance with the definition of the outgoing
%message when the incoming "message" is a delta function at
%x_t. This conditional density is computed using the leaf
%expression of Song, Gretton, and Guestrin, Nonparametric Tree
%Graphical Models via Kernel Embeddings, AISTATS 2010, Section 5.

for leafInd=1:length(observedList)
  whichLeaf = observedList(leafInd);
 
  nghbrInd = find(Am(:,whichLeaf)>0);
  for whichNghbr = nghbrInd'
    Kt = Karr{whichLeaf};
    Ks = Karr{whichNghbr};
    I = eye(size(Kt));

    kt = rbf_dot( obsTrainArr{whichLeaf},observations(leafInd),msgPrm.sig );

    betaArr{whichLeaf}{whichNghbr} =  ((Kt + msgPrm.lambda*I)*(Ks+msgPrm.lambda*I)) \kt ;
    %normalize for numerical stability:
    betaArr{whichLeaf}{whichNghbr} = betaArr{whichLeaf}{whichNghbr}/...
        norm(betaArr{whichLeaf}{whichNghbr});
  end
end



%Initialize the BETAS to be constant vectors with unit norm.

for nodeInd = setdiff(1:numNodes,observedList)

  nghbrInd = find(Am(nodeInd,:)>0);
  for outLinkInd = nghbrInd
    betaArr{nodeInd}{outLinkInd} = ones(n,1)/sqrt(n);
  end
end


%Precompute the regularized inverse Gram matrices

disp('** Precomputing regularized inverse Gram matrices **')
for nodeInd = setdiff(1:numNodes,observedList)
   KarrInvReg{nodeInd} = inv( Karr{nodeInd}  + msgPrm.lambda*eye(n));
end
    

%Compute INTERNAL messages
%Here the schedule is to iterate over nodes. 

for whichLoop = 1:msgPrm.numIter
  
  fprintf('BP iteration: %i\n',whichLoop);
  
  %iterate over all unobserved nodes
  for nodeInd = setdiff(1:numNodes,observedList)

    nghbrInd = find(Am(nodeInd,:)>0);
    
    %don't compute messages to observed nodes
    
      
    for outMessageInd = setdiff(nghbrInd,observedList)
      %compute incoming message from all nodes besides the one which will
      %receive the outgoing message
      
      Ktu_Beta = ones(n,1);  %The vector K * \beta
  
      for inMessageInd = setdiff(nghbrInd,outMessageInd)
    
       

        Ktu_Beta = Ktu_Beta.* (Karr{nodeInd}*...
                               betaArr{inMessageInd}{nodeInd});
                       
      
      end
      betaArr{nodeInd}{outMessageInd} = KarrInvReg{outMessageInd} * Ktu_Beta ;
      %for numerical stability: rescale betas to have unit norm
      betaArr{nodeInd}{outMessageInd} =betaArr{nodeInd}{outMessageInd}/...
        norm(betaArr{nodeInd}{outMessageInd});
    
    end
    
  end  
end

